# http://beamusup.com/generate-htaccess/

RewriteEngine on

#############################
# Force all traffic to HTTPS
RewriteCond %{HTTPS} off
RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]

######################
# Site map for humans
# Die Domain ändern!
RedirectMatch 301 (?i)^/sitemap.html? http://domain.tld/sitemap/

###############################
# Redirect to HTTP without www
# RewriteCond %{HTTPS} off
# RewriteCond %{HTTP_HOST} ^www\.(.*)$ [NC]
# RewriteRule .* http://%1%{REQUEST_URI} [R=301,L]

#################################
# Redirect to HTTPS without www
RewriteCond %{HTTPS} on
RewriteCond %{HTTP_HOST} ^www\.(.*)$ [NC]
RewriteRule .* https://%1%{REQUEST_URI} [R=301,L]

##################
# Stop hotlinking
RewriteCond %{HTTP_REFERER} !^$
RewriteCond %{HTTP_REFERER} ^https?://([^/]+)/ [NC]
RewriteCond %1#%{HTTP_HOST} !^(.+)#\1$
RewriteRule \.(jpg|jpeg|png|gif|swf|svg)$ - [NC,F,L]

##############
# Add Caching
<FilesMatch ".(ico|jpg|jpeg|png|gif|js|css|swf)$">
    Header set Cache-Control "max-age=2592000"
</FilesMatch>

#####################
# Custom error pages
ErrorDocument 404 /404.html

###################################
# Prevent viewing of htaccess file
<Files .htaccess>
    order allow,deny
    deny from all
</Files>

#############################
# Prevent directory listings
Options All -Indexes

#############################
#  GZIP
# mod_gzip compression (legacy, Apache 1.3)
<IfModule mod_gzip.c>
    mod_gzip_on Yes
    mod_gzip_dechunk Yes
    mod_gzip_item_include file \.(html?|xml|txt|css|js)$
    mod_gzip_item_include handler ^cgi-script$
    mod_gzip_item_include mime ^text/.*
    mod_gzip_item_include mime ^application/x-javascript.*
    mod_gzip_item_exclude mime ^image/.*
    mod_gzip_item_exclude rspheader ^Content-Encoding:.*gzip.*
</IfModule>

#############################
# DEFLATE compression
<IfModule mod_deflate.c>
    # Set compression for: html,txt,xml,js,css
    AddOutputFilterByType DEFLATE text/html text/plain text/xml application/xml application/xhtml+xml text/javascript text/css application/x-javascript
    # Deactivate compression for buggy browsers
    BrowserMatch ^Mozilla/4 gzip-only-text/html
    BrowserMatch ^Mozilla/4.0[678] no-gzip
    BrowserMatch bMSIE !no-gzip !gzip-only-text/html
    # Set header information for proxies
    Header append Vary User-Agent
</IfModule>

+++
title        = "Contact"
date         = 2017-07-14T15:40:34+01:00
layout       = "contact"
description  = "This description is from contact.md"
showMap      = "yes"
bodyclass    = "contact"

headline     = ""

[menu.main]
weight = 10

[menu.footer]
weight = 5
+++

This text is in `./content/contact.md`. This file has it's own layout `./layouts/_default/contact.html` which is chosen by Hugo automatically due to the same name as the file. The file is named `contact.md` and the layout `./layouts/_default/contact.html`).

In layout `./layouts/_default/contact.html` the partial `./layouts/partials/map/mapContainer.html` is called.

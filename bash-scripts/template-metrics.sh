#!/usr/bin/env bash

clear

echo
echo "--------------------------------------------------------------------------------"
echo "This script tests the Hugo installation for performance and recommends"
echo "for caching."
echo "--------------------------------------------------------------------------------"
echo

hugo --templateMetrics --templateMetricsHints

echo
exit 0
